# applications

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### 测试服务器上传
scp -r dist root@192.168.0.170:/data/chainmaker-app/
scp -r dist root@82.157.129.254:/data/soft/evidence-testnet

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
