// Generated by 'unplugin-auto-import'
// We suggest you to commit this file into source control
declare global {
  const ElAlert: typeof import('element-plus/es')['ElAlert']
  const ElButton: typeof import('element-plus/es')['ElButton']
  const ElCard: typeof import('element-plus/es')['ElCard']
  const ElCol: typeof import('element-plus/es')['ElCol']
  const ElConfigProvider: typeof import('element-plus/es')['ElConfigProvider']
  const ElDialog: typeof import('element-plus/es')['ElDialog']
  const ElEmpty: typeof import('element-plus/es')['ElEmpty']
  const ElForm: typeof import('element-plus/es')['ElForm']
  const ElFormItem: typeof import('element-plus/es')['ElFormItem']
  const ElIcon: typeof import('element-plus/es')['ElIcon']
  const ElInput: typeof import('element-plus/es')['ElInput']
  const ElPopover: typeof import('element-plus/es')['ElPopover']
  const ElRow: typeof import('element-plus/es')['ElRow']
  const ElUpload: typeof import('element-plus/es')['ElUpload']
}
export {}
