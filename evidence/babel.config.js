/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',
  ],
};
