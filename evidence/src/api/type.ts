/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { AxiosResponse } from 'axios';

export interface PageParam {
  pageNum: number;
  pageSize: number;
}
export interface CommonResponse{
  code: number;
  msg: string;
}
export interface ResponseData<T> extends CommonResponse{
  Data: T;
}
export interface ResponseList extends CommonResponse{
  totalCount: number;
}

export interface ResponseError {
  Error: {
    Code: string;
    Message: string;
  };
}
export interface ResponseIf<T> {
  Response: T;
}
// export type ResponseInfo<T> = AxiosResponse<ResponseData<T> | ResponseList<T> | ResponseError>;
export type Fetch<P, T> = (params: P) => Promise<T>;
