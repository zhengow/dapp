/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from './App.vue';
import router from './router';

import filter from './filter';
import './index.css';
import 'element-plus/dist/index.css';
import extensionListener from './utils/extensionListener';

const app = createApp(App);
app.use(createPinia());
app.use(filter).use(router).mount('#app');
extensionListener();
