# 简介
该前端项目为"长安链画作NFT创作平台"对应DAPP，依赖IPFS存储图片。

## 前端安装和部署

### 依赖安装
```
npm install
```

### 配置修改
```
.env.* 配置文件对应对应修改为部署机器的弹性IP
VUE_APP_IPFS_HOST_API_HOST={ip}
VUE_APP_IPFS_HOST_IMAGE_HOST=http://{ip}:8080/ipfs/

```


### 本地开发运行
```
npm run dev
```


### 前端编译
```
npm run build
```

### 快速格式化
```
npm run lint
```

## IPFS安装和启动

### 安装
#### 下载压缩包
``` 
wget https://dist.ipfs.tech/kubo/v0.16.0/kubo_v0.16.0_linux-amd64.tar.gz 
```
#### 解压
``` 
tar -xvzf kubo_v0.16.0_linux-amd64.tar.gz
```
#### 安装
``` 
cd kubo
sudo bash install.sh

> Moved ./ipfs to /usr/local/bin
```
#### 下载压缩包
``` 
wget https://dist.ipfs.tech/kubo/v0.16.0/kubo_v0.16.0_linux-amd64.tar.gz
```

### 初始化
```
ipfs init 
```

### 配置修改
```
ipfs config Addresses.Gateway /ip4/0.0.0.0/tcp/8080 // 允许外网访问图片
ipfs config Addresses.API /ip4/0.0.0.0/tcp/5001 // 允许外网访问接口
IPFS 设置【跨域请求】
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["*"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["GET", "POST"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Headers '["Authorization"]'
ipfs config --json API.HTTPHeaders.Access-Control-Expose-Headers '["Location"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Credentials '["true"]'
```

### 启动守护进程
```
ipfs daemon 
```